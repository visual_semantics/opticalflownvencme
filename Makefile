include ./common.mk
LDFLAGS = -lswscale -lavdevice -lavformat -lavcodec -lswresample -lavutil -lpthread -lbz2 -lz -lc -lrt

# Target rules
all: build

build: MotionEstimation

NvEncoder.o: ./NvCodec/NvEncoder/NvEncoder.cpp ./NvCodec/NvEncoder/NvEncoder.h
	$(GCC) $(CCFLAGS) $(INCLUDES) -o $@ -c $<

NvEncoderCuda.o: ./NvCodec/NvEncoder/NvEncoderCuda.cpp ./NvCodec/NvEncoder/NvEncoderCuda.h \
                 ./NvCodec/NvEncoder/NvEncoder.h
	$(GCC) $(CCFLAGS) $(INCLUDES) -o $@ -c $<


MotionEstimation.o: MotionEstimation.cpp ./NvCodec/NvEncoder/NvEncoderCuda.h \
            ./NvCodec/NvEncoder/NvEncoder.h ./Utils/NvCodecUtils.h \
            ./Utils/NvEncoderCLIOptions.h ./Utils/Logger.h
	$(GCC) $(CCFLAGS) $(INCLUDES) -o $@ -c $<

MotionEstimation: MotionEstimation.o NvEncoder.o NvEncoderCuda.o
	$(GCC) $(CCFLAGS) -o $@ $^ $(LDFLAGS)

vis: vis.cpp
	g++ $< -o $@ $(CFLAGS) -lopencv_highgui -lopencv_videoio -lopencv_imgproc -lopencv_imgcodecs -lopencv_core -lpng $(LDFLAGS) $(INSTALLED_DEPS) 

clean:
	rm -rf MotionEstimation MotionEstimation.o NvEncoder.o NvEncoderCuda.o

